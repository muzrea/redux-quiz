const initialState = {
  notes: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "GET_ALL_NOTES":
      return {
        ...state,
        notes: action.notes
      };
    default:
      return state;
  }
}