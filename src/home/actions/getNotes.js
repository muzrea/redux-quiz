export const getNotes = () => (dispatch) => {
  fetch("http://localhost:8080/api/posts",{method: 'get'})
    .then(res => res.json())
    .then(result => {
        dispatch({
          type: "GET_ALL_NOTES",
          notes: result
        })
      }
    )
};