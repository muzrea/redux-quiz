import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import {bindActionCreators} from "redux";
import {getNotes} from "../actions/getNotes";
import "./home.less";
import {Link} from "react-router-dom";

class Home extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.getNotes();
  }

  render() {
    const {notes} = this.props.allNotes;
    const items = notes.map((item, index) => {
        const url = `/notes/${item.id}`;
        return <Link to={url} className='note-title' key={`noteTitle${index}`}>{item.title}</Link>;
      }
    );
    return (
      <div className='home'>
        <main className='main'>
          {items}
          <Link to='/note/create' className='note-title'>+</Link>
        </main>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  allNotes: state.createNoteReducer
});
const mapDispatchToProps = (dispatch) => bindActionCreators({getNotes}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Home);
