const initialState = {
  noteDetails: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "GET_NOTE_DETAILS":
      return {
        ...state,
        notes: action.noteDetails
      };
    default:
      return state;
  }
}