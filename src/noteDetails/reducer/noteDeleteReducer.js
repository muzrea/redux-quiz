const initialState = {
  notes: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "DELETE_NOTE":
      return {
        ...state,
        notes: action.notes
      };
    default:
      return state;
  }
}