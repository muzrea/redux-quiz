export const deleteNote = (id,callback) => (dispatch) => {
  fetch(`http://localhost:8080/api/posts/${id}`,{method: 'delete'})
    .then(() => {
      dispatch({
        type: "DELETE_NOTE"
      });
    })
    .then(callback)
    .catch(callback)
};