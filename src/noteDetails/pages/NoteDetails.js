import React, {Component} from 'react';
import {bindActionCreators} from "redux";
import connect from "react-redux/es/connect/connect";
import {Link} from "react-router-dom";
import {getNotes} from "../../home/actions/getNotes";
import "./nodeDetails.less"
import {deleteNote} from "../actions/deteleNote";

class NoteDetails extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.getNotes();
  }

  deleteNote() {
    this.props.deleteNote(this.props.match.params.id,this.props.getNotes);
  }

  render() {
    const {notes} = this.props.allNotes;
    const id = this.props.match.params.id;
    const items = notes.map((item, index) => {
        const url = `/notes/${item.id}`;
        return <div className='aside-title'><Link to={url} key={`noteTitle${index}`}>{item.title}</Link></div>;
      }
    );
    return (
      <main className='detail-main'>
        <aside className='aside'>
          {items}
        </aside>
        <article>
          <h2>{notes.find(item => item.id == id).title}</h2>
          <hr/>
          <div className='desc'>
            {notes.find(item => item.id == id).description}
          </div>
          <Link to='/' onClick={this.deleteNote.bind(this)}>删除</Link>
        </article>
      </main>
    )
  }
}

const mapStateToProps = (state) => ({
  allNotes: state.createNoteReducer
});
const mapDispatchToProps = (dispatch) => bindActionCreators({getNotes, deleteNote}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(NoteDetails);