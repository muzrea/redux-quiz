import React, {Component} from 'react';
import './App.less';
import Home from "./home/pages/Home";
import {BrowserRouter as Router, Link} from "react-router-dom";
import {Switch, Route} from "react-router";
import NoteDetails from "./noteDetails/pages/NoteDetails";
import NewNote from "./noteCreator/pages/NewNote";
import {IoMdToday} from 'react-icons/io';

class App extends Component {

  render() {
    return (
      <Router>
        <div className='App'>
          <header className='header'>
            <Link exact to='/'>
              <IoMdToday className='icon'/>
            </Link>
            <span className='title-text'>Notes</span>
          </header>
          <Switch>
            <Route path="/notes/:id" component={NoteDetails}/>
            <Route path="/note/create" component={NewNote}/>
            <Route exact path="/" component={Home}/>
          </Switch>
        </div>
      </Router>

    );
  }
}

export default App;