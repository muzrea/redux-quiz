import {combineReducers} from "redux";
import createNoteReducer from "../home/reducer/createNoteReducer";
import noteDetailReducer from "../noteDetails/reducer/noteDetailReducer";
import deleteNoteReducer from "../noteDetails/reducer/noteDeleteReducer";
import noteAddReducer from "../noteCreator/reducer/noteAddReducer";



const reducers = combineReducers({
  createNoteReducer: createNoteReducer,
  noteDetailReducer: noteDetailReducer,
  deleteNoteReducer: deleteNoteReducer,
  noteAddReducer: noteAddReducer
});
export default reducers;