export const addNote = (title,description,callback) => (dispatch) => {
  const body = {"title": title,"description": description};
  fetch('http://localhost:8080/api/posts/', {
    method: 'post',
    headers: {'Content-Type':'application/json'},
    body: JSON.stringify(body)
  })
    .then(callback);
};