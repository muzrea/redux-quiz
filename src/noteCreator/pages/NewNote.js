import React, {Component} from 'react';
import {bindActionCreators} from "redux";
import connect from "react-redux/es/connect/connect";
import {addNote} from "../actions/addNote";
import {Link} from "react-router-dom";
import {getNotes} from "../../home/actions/getNotes";

class NewNote extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      title: '',
      description: ''
    })
  }

  changeTitle(event) {
    this.setState({
      title: event.target.value
    })
  }

  changeDescription(event) {
    this.setState({
      description: event.target.value
    })
  }

  handleSubmit(){
    this.props.addNote(this.state.title,this.state.description,this.props.getNotes);
  }

  handleCancel(){
    this.setState({
      title: '',
      description: ''
    })
  }

  render() {
    return (
      <div>
        <h1>创建笔记</h1>
        <hr/>
        <label>
          标题
          <input onChange={this.changeTitle.bind(this)} type='text' value={this.state.title}/>
        </label>
        <label>
          正文
          <textarea onChange={this.changeDescription.bind(this)} value={this.state.description}></textarea>
        </label>
        <Link exact to='/' onClick={this.handleSubmit.bind(this)}>提交</Link>
        <button onClick={this.handleCancel.bind(this)}>取消</button>
      </div>
    )
  }
}


const mapStateToProps = (state) => ({
  allNotes: state.noteAddReducer
});
const mapDispatchToProps = (dispatch) => bindActionCreators({getNotes,addNote}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(NewNote);